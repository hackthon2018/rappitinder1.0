package rapihack.com.co.rappihack.datasource;

import java.util.List;

public interface RemoteDataSource {

    void consultar(GetCallback callback);
    void guardar(PostCallback callback, Object obj);

    public interface GetCallback{
        void onLoaded(List<Object> obj);
        void onError();
    }

    public interface PostCallback{
        void guardarData();
        void onError();
    }
}
