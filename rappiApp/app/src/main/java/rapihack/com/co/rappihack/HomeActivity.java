package rapihack.com.co.rappihack;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import rapihack.com.co.rappihack.dominio.Dominio;
import rapihack.com.co.rappihack.vista.adapter.DataAdapter;

public class HomeActivity extends AppCompatActivity implements Dominio.View{
    private DataAdapter adapter;
    private Dominio dominio;
    private RecyclerView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        lista = (RecyclerView) findViewById(R.id.lista);
    }


    @Override
    public void show(List<Object> obj) {
        adapter = new DataAdapter(getApplicationContext(), obj, this);
        lista.setAdapter(adapter);
    }

    @Override
    public void showMensaje(int mensaje) {
        Log.d("Error", getString(mensaje));
    }
}
