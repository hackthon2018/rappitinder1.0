package rapihack.com.co.rappihack.datasource;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;

import java.io.UnsupportedEncodingException;

import java.util.List;

public class DataSource implements RemoteDataSource {

    private final static String URL_API = "https://api-movil.herokuapp.com/api/history";
    private Context conext;
    private RequestQueue requestQueue;
    private static RemoteDataSource INSTANCE = null;

    private DataSource(Context context){
        this.conext = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public static RemoteDataSource getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new DataSource(context);
        }

        return INSTANCE;
    }

    @Override
    public void consultar(final GetCallback callback) {
        try{
            JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                URL_API, null, new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response){
                        try{
                            Gson gson = new Gson();
                            List<Object> objs = (List<Object>) gson.fromJson(response.toString(), new TypeToken<List<Object>>(){
                            }.getType());
                            callback.onLoaded(objs);
                        }catch(Exception e){
                            callback.onError();
                        }
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        callback.onError();
                    }
                }
            );

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void guardar(final PostCallback callback, Object obj) {
        try{
            StringRequest postRequest = new StringRequest(Request.Method.POST, URL_API,
                    new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response){
                            callback.guardarData();
                        }
                    },
                    new Response.ErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error){
                            callback.onError();
                        }
                    }
            ){
                @Override
                public String getBodyContentType(){return "application/json; charset=utf-0";}

                @Override
                public byte[] getBody() throws AuthFailureError{
                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(obj);
                    byte[] body = new byte[0];

                    try{
                        body = json.getBytes("utf-8");
                    }catch(UnsupportedEncodingException e){
                        body = new byte[0];
                    }

                    return body;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response){
                    String responseString ="";

                    if(response != null){
                        responseString = String.valueOf(response.statusCode);
                    }
                    return Response.success(responseString, HttpHeaderParser.parserCacheHeaders(response));
                }
            };
            requestQueue.add(postRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
