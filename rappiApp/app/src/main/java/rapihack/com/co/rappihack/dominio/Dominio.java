package rapihack.com.co.rappihack.dominio;


import java.util.List;

import rapihack.com.co.rappihack.datasource.RemoteDataSource;

public class Dominio {

    private View view;
    private RemoteDataSource remoteDataSource;

    public Dominio(View view, RemoteDataSource remoteDataSource){
        this.view = view;
        this.remoteDataSource = remoteDataSource;
    }

    public void consultarDatos(){
        remoteDataSource.consultar(new RemoteDataSource.GetCallback() {
            @Override
            public void onLoaded(List<Object> obj) {
                view.show(obj);
            }

            @Override
            public void onError() {
                view.showMensaje(0);
            }
        });
    }

    public void guardarDdatos(Object obj){
        remoteDataSource.guardar(new RemoteDataSource.PostCallback() {
            @Override
            public void guardarData() {
              view.showMensaje(0);
            }

            @Override
            public void onError() {
                view.showMensaje(0);
            }
        }, obj);
    }

    public interface View{
        void show(List<Object> obj);
        void showMensaje(int mensaje);
    }

}
