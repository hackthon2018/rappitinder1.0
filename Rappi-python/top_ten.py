
# coding: utf-8

# In[188]:


import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
import json

#Lectura de datos de entrada
products = pd.read_json("products.json")
data = pd.read_json("data_user.json") #Leyendo los datos del nuevo usuario

#Procesamiento, creación y limpieza de los datos
products = pd.DataFrame(np.unique(products["name"].values), columns=["Product"])
binary_values = pd.DataFrame(np.random.randint(0,2,size=(830,3)),columns=["Gender","Vegetarian","Alcohol"])
age = pd.DataFrame(np.random.randint(0,101,size=(830,1)), columns=["Age"])
tastes = pd.DataFrame(np.random.randint(0,5,size=(830,1)),columns=["Tastes"])
df = pd.concat([binary_values,age,tastes,products], axis=1)
Encoder = LabelEncoder()
df["Product"] = Encoder.fit_transform(df["Product"])

#Creando los datos aleatorios
df = df.sample(16400, replace=True,random_state=42)

#Creaciòn del arbol de decisión entregando las 10 primeras preferencias de productos
results = []
for i in range(11):
    X = df.iloc[:,0:5]
    y = df["Product"]
    X_train, X_test, y_train, y_test = train_test_split(X, y)
    tree = DecisionTreeClassifier()
    tree.fit(X_train,y_train)
    y_predict = tree.predict(X_test)
    predict = tree.predict(data.transpose().values)
    result = predict[0]
    results.append(result)
    df = df[df["Product"]!=result]

#Guardando los resultados en un json
data = {}
data['data'] = results
with open('top_ten.json', 'w') as outfile:
    json.dump(data, outfile)

