'use strict'
 const mongojs = require('mongojs');
 const cors = require('cors');

const router = require('express').Router()
const Product = require('../models/product')
const db = mongojs('mongodb://adminMovil08642:9753124680Root@ds227352.mlab.com:27352/db_salud')

router.post('favorite',(req,res,next)=>{
	var favorite;
	favorite.user = req.body.user;
	favorite.idP = req.body.idP; 

	db.fav.save(favorite, (err,fav)=>{
		if(err)return next(err);
		res.status(200).json(fav);
	});
})

router.put('/favorite/:id', (req,res,next)=>{
	const favorite = req.body;

	db.fav.update({_id: mongojs.ObjectId(req.params.id)},{$set: favorite},(err,cite)=>{
		if(err){
		 res.status(400).json('error',err);
		 return next(err);}
		res.status(200).json({result:favorite});
	})
})

module.exports = router;