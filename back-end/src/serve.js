// imports
const express = require('express');
const path = require('path');
const cors = require('cors');
const app = express();
const mongoose = require('mongoose')



//const routes = require('./routes/index');

const rproducts = require('./routes/products');
//const userR = require('./routes/users');

// settings
//app.set('views', path.join(__dirname, 'views'));
app.set('port', process.env.PORT || 3000);
app.engine('html', require('ejs').renderFile);
app.set('view engines','ejs');

// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// routes
//app.use(routes);

app.use('/api',rproducts);

// static files
//app.use(express.static(path.join(__dirname,'dist/client')));

// star serve
app.listen(app.get('port'),()=>{
	console.log('serve on ',app.get('port'))
});

//conection mongodb
 mongoose.connect('mongodb://hackaton_med:rappi.rulz@mongo-hackathon.eastus2.cloudapp.azure.com:27017/rappi',
 	(err, res)=>{
 		if (err) {
 			return console.log(`error al conectar ${err}`)
 		}
 		console.log(`coneccion a la DB exitosa `)
 })

 const mongojs = require('mongojs');

const db = mongojs('mongodb://hackaton_med:rappi.rulz@mongo-hackathon.eastus2.cloudapp.azure.com:27017/rappi')

const Product = require('./models/product')

// Product.find({}, function (err, docs) {
// 		if (err) { res.status(500).json({ message : 'Error en el servidor' }) }

// 		if(docs){ console.log(docs) }
// 	})
